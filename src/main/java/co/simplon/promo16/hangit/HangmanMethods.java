package co.simplon.promo16.hangit;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class HangmanMethods {

    private Scanner dico;

    private List<String> wordsToFind = new ArrayList<>();
    private Random randomWord = new Random();
    private int randomTry;
    private String wordToFindFromLib;
    private Scanner keyboard = new Scanner(System.in);
    private boolean over;
    private List<Character> playerCharac = new ArrayList<>();
    int countToTheDeath = 0;

    public HangmanMethods() {
        try {
            dico = new Scanner(new File(
                    "/home/anjoystick/Documents/jeux_pendus/jeux_pendus/src/main/java/co/simplon/promo16/hangit/dico.txt"));
            while (dico.hasNextLine()) {
                wordsToFind.add(dico.nextLine());

            }
            randomTry = randomWord.nextInt(wordsToFind.size());
            wordToFindFromLib = wordsToFind.get(randomTry);

        } catch (FileNotFoundException e) {

            e.printStackTrace();
        }
    }

    /**
     * permet de choisir le nombre de joueur
     */
    public void playerCount() {
        System.out.println("Prêts à jouer ? \n 1: Pour oui");
        String players = keyboard.nextLine();
        if (players.equals("1")) {
            for (int i = 0; i < wordToFindFromLib.length(); i++) {
                System.out.print("-");
            }
            System.out.println("\n");
            loopingTemp();
        }
      //   else {
      //      System.out.println("Entrez le mot à deviner : ");
      //      wordToFindFromLib = keyboard.nextLine();
      //      System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
      //      System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
      //      System.out.println("Joueur 2 c'est à toi !\n");
      //      for (int i = 0; i < wordToFindFromLib.length(); i++) {
      //          System.out.print("-");
      //      }
      //      System.out.println("\n");
      //      loopingTemp();
      //  }
      //Tentative de deuxième joueur a finir d'implanté
    }

    /**
     * Affiche l'état du mot a rechercher en fonction des lettre rentré par le
     * joueur
     * 
     * 
     */
    public boolean gameCurrentState(String wordToFindFromLib, List<Character> playerCharac) {
        int countToTheWin = 0;
        boolean testover = false;
        for (int i = 0; i < wordToFindFromLib.length(); i++) {
            if (playerCharac.contains(wordToFindFromLib.charAt(i))) { // comparé la lettre entré par le joueur avec les
                                                                      // lettre du mot a trouvé en fonction de l'index
                                                                      // actuel
                System.out.print(wordToFindFromLib.charAt(i));
                countToTheWin++;
                if (playerCharac.get(playerCharac.size() - 1).equals(wordToFindFromLib.charAt(i))) // vérifie que la
                                                                                                   // dernière lettre
                                                                                                   // stocké dans la
                                                                                                   // liste de lettre
                                                                                                   // rentré par le
                                                                                                   // joueur, est
                                                                                                   // mauvaise
                { // j'ai du faire sa a cause de ma facon de récupérer l'input du joueur en le
                  // mettant dans une liste
                    testover = true; // sinon des qu'une lettre fausse était rentré la vérification précédente la
                                     // détectait constamment dans la liste est incrémentait le compteur de mort.
                }
            } else {
                System.out.print("-");
            }
        }
        if (!testover)
            countToTheDeath++;
        System.out.println("");
        return (countToTheWin == wordToFindFromLib.length()); // renvoi true si le compteur de victoire est égale a la
                                                              // taille du mot
    }

    /**
     * boucle pour vérifier les conditions de victoire et défaite
     * et faire l'affichage du pendus
     */
    public void loopingTemp() {

        while (!over) {
            String stockTemp = playerInput(wordToFindFromLib, playerCharac, keyboard);
            if (gameCurrentState(wordToFindFromLib, playerCharac)) { //
                System.out.println("Gratz!");
                break;
            }
            if (stockTemp.equals(wordToFindFromLib)) { // permet au joueur de rentré le mot en entier s'il la deviné et
                                                       // de gagner
                System.out.println("Gratz!");
                break;
            }
            if (countToTheDeath == 1) {
                System.out.println("\n=========");
            }
            if (countToTheDeath == 2) {
                System.out.println("""
                        \n
                         |
                         |
                         |
                         |
                         |
                        =========
                        """);
            }
            if (countToTheDeath == 3) {
                System.out.println("""
                         \n
                          _______
                         |
                         |
                         |
                         |
                         |
                        =========
                        """);
            }
            if (countToTheDeath == 4) {
                System.out.println("""
                         \n
                          _______
                         |       |
                         |
                         |
                         |
                         |
                        =========
                        """);
            }
            if (countToTheDeath == 5) {
                System.out.println("""
                         \n
                          _______
                         |       |
                         |       O
                         |
                         |
                         |
                        =========
                        """);
                System.out.println("Dernière vie");
            }
            if (countToTheDeath == 6) {
                System.out.println("""
                         \n
                          _______
                         |       |
                         |       O
                         |      /|\\
                         |      / \\
                         |
                        =========
                         """);
                System.out.println("Game Over");
                System.out.println("Le mot a trouver était : " + wordToFindFromLib);
                break;
            }
        }
    }

    /**
     * récupère la lettre rentré par le joueur
     * et l'envoie dans le tableau prévus pour
     */
    public String playerInput(String wordToFindFromLib, List<Character> playerCharac, Scanner keyboard) {
        System.out.println("Entrez une lettre :");
        String playerInput = keyboard.nextLine().toUpperCase();
        if (playerInput.length() > 0) {
            playerCharac.add(playerInput.charAt(0));
        }
        return playerInput;

    }

    /**
     * méthode qui lance les autres
     */
    public void bindThemAll() {
        opening();
        playerCount();

    }

    public void opening() {
        System.out.println("""
                \n
                888
                888
                888
                88888b.  8888b. 88888b.  .d88b. 88888b.d88b.  8888b. 88888b.
                888 "88b    "88b888 "88bd88P"88b888 "888 "88b    "88b888 "88b
                888  888.d888888888  888888  888888  888  888.d888888888  888
                888  888888  888888  888Y88b 888888  888  888888  888888  888
                888  888"Y888888888  888 "Y88888888  888  888"Y888888888  888
                                             888
                                        Y8b d88P
                                         "Y88P"
                """);
        System.out.println(
                "****Bienvenue dans le jeux du Pendus !****\n*****Vous devez deviner le mot afin d'éviter d'être pendus!*****\n*****Vous avez 6 vies!*****");
        System.out.println("");
    }
}
