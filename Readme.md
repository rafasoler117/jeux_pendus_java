# Le jeux du pendu

Pour mon projet de jeux, j'ai choisis le jeux du pendu.
Il s'agit de trouver un mot avec pour règle que chaque erreur nous 
coûte une vie, avec une base de 6.

Pour réaliser mon pendu, j'ai commencé par créer les différentes classes
dont j'avais besoins:

* Un tableau pour les mots a trouver
* initialiser la fonction Random qui en choisiras un au hasard
* le scanner pour récupérer l'input du joueur
* un boolean pour la boucle qui permettras de jouer jusqu'a la victoire ou la défaite
* une liste dans laquelle je stock les différentes lettres utilisé par le joueur
* un compteur de bonne et mauvaise réponse

Le programme est composé de différente fonction s'appelant les une les autres : 

* Une fonction pour l'affichage de la présentation  du jeux et de ses règles
* Une fonction pour récupérer l'input du joueur et la stocké dans la liste prévu pour
* Une fonction pour choisir le nombre de joueur
* Une fonction pour afficher le mot caché, qui se mettra à jour au fur et à mesure du jeux
* Une fonction pour les conditions de victoire et de défaite ainsi que l'affichage du pendu
* Une fonction qui apelle les autres afin de n'avoir que celle la à appelé pour lancé le jeux